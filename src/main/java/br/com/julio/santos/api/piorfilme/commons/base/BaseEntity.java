package br.com.julio.santos.api.piorfilme.commons.base;

import java.io.Serializable;

public abstract class BaseEntity<T> implements Serializable {

    private static final long serialVersionUID = -8629340562772756433L;

    public abstract T getId();
}

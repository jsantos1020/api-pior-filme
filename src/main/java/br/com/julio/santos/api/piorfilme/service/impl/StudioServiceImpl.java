package br.com.julio.santos.api.piorfilme.service.impl;

import br.com.julio.santos.api.piorfilme.model.Movie;
import br.com.julio.santos.api.piorfilme.model.MovieStudio;
import br.com.julio.santos.api.piorfilme.model.Studio;
import br.com.julio.santos.api.piorfilme.repository.StudioRepository;
import br.com.julio.santos.api.piorfilme.service.api.MovieStudioService;
import br.com.julio.santos.api.piorfilme.service.api.StudioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StudioServiceImpl implements StudioService {

    @Autowired
    private StudioRepository repository;
    @Autowired
    private MovieStudioService movieStudioService;

    @Transactional
    @Override
    public Studio save(Studio entity) {
        return repository.save(entity);
    }

    @Transactional
    @Override
    public void saveStudioAndMovie(Movie movie, String studios) {
        for (String str : studios.split(",|\\ and ")) {
            Studio studio = checkStudio(str.trim());
            movieStudioService.save(new MovieStudio(movie, studio));
        }
    }

    @Transactional
    @Override
    public List<String> saveStudioAndMovie(Movie movie, List<String> studios) {
        List<String> list = new ArrayList<>();
        for (String name : studios) {
            Studio studio = checkStudio(name);
            movieStudioService.save(new MovieStudio(movie, studio));
            list.add(studio.getName());
        }
        return list;
    }

    private Studio checkStudio(String name) {
        Optional<Studio> studioOptional = repository.findByName(name);
        if (studioOptional.isPresent()) {
            return studioOptional.get();
        } else {
            return repository.save(new Studio(name));
        }
    }
}

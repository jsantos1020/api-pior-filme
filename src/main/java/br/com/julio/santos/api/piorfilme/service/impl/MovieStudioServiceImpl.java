package br.com.julio.santos.api.piorfilme.service.impl;

import br.com.julio.santos.api.piorfilme.model.MovieStudio;
import br.com.julio.santos.api.piorfilme.repository.MovieStudioRepository;
import br.com.julio.santos.api.piorfilme.service.api.MovieStudioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MovieStudioServiceImpl implements MovieStudioService {

    @Autowired
    private MovieStudioRepository repository;

    @Override
    public MovieStudio save(MovieStudio entity) {
        return repository.save(entity);
    }
}

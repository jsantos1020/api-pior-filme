package br.com.julio.santos.api.piorfilme.service.impl;

import br.com.julio.santos.api.piorfilme.model.Movie;
import br.com.julio.santos.api.piorfilme.model.MovieProducer;
import br.com.julio.santos.api.piorfilme.model.Producer;
import br.com.julio.santos.api.piorfilme.repository.ProducerRepository;
import br.com.julio.santos.api.piorfilme.service.api.MovieProducerService;
import br.com.julio.santos.api.piorfilme.service.api.ProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProducerServiceImpl implements ProducerService {

    @Autowired
    private ProducerRepository repository;
    @Autowired
    private MovieProducerService movieProducerService;

    @Transactional
    @Override
    public Producer save(Producer entity) {
        return repository.save(entity);
    }

    @Transactional
    @Override
    public void saveProducerAndMovice(Movie movie, String producers) {
        for (String str : producers.split(",|\\ and ")) {
            Producer producer = checkProducer(str.trim());
            movieProducerService.save(new MovieProducer(movie, producer));
        }
    }

    @Transactional
    @Override
    public List<String> saveProducerAndMovice(Movie movie, List<String> producers) {
        List<String> list = new ArrayList<>();
        for (String name : producers) {
            Producer producer = checkProducer(name);
            movieProducerService.save(new MovieProducer(movie, producer));
            list.add(producer.getName());
        }
        return list;
    }

    private Producer checkProducer(String name) {
        Optional<Producer> producerOptional = repository.findByName(name);
        if (producerOptional.isPresent()) {
            return producerOptional.get();
        } else {
            return repository.save(new Producer(name));
        }
    }
}

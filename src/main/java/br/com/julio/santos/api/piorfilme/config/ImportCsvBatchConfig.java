package br.com.julio.santos.api.piorfilme.config;

import br.com.julio.santos.api.piorfilme.model.Movie;
import br.com.julio.santos.api.piorfilme.service.api.MovieService;
import br.com.julio.santos.api.piorfilme.service.api.ProducerService;
import br.com.julio.santos.api.piorfilme.service.api.StudioService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

@Configuration
public class ImportCsvBatchConfig {

    @Autowired
    private MovieService movieService;
    @Autowired
    private StudioService studioService;
    @Autowired
    private ProducerService producerService;

    @Bean
    public CommandLineRunner importCsvBatch(ApplicationContext appContext) {
        return args -> {

            InputStream in = new FileInputStream("src/main/resources/csv/movielist.csv");
            Reader reader = new InputStreamReader(in);
            Iterable<CSVRecord> records = CSVFormat.RFC4180
                    .withDelimiter(';')
                    .withHeader("year","title","studios","producers","winner")
                    .parse(reader);

            for (CSVRecord record : records) {
                if (record.getRecordNumber() == 1) {
                    continue;
                }

                String winner = record.get("winner");
                Movie movie = movieService.save(Movie.builder().year(Integer.valueOf(record.get("year"))).title(record.get("title")).winner("yes".equalsIgnoreCase(winner)).build());

                String studios = record.get("studios");
                studioService.saveStudioAndMovie(movie, studios);

                String producers = record.get("producers");
                producerService.saveProducerAndMovice(movie, producers);
            }
        };
    }
}

package br.com.julio.santos.api.piorfilme.controller;

import br.com.julio.santos.api.piorfilme.dto.ProducerIntervalAwardsDto;
import br.com.julio.santos.api.piorfilme.service.api.MovieProducerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/producers")
@Api(tags = { "Producer" }, description = "URI for the Producer feature")
public class ProducerController {

    @Autowired
    private MovieProducerService service;

    @ApiOperation(value = "It seeks the maximum and minimum range of prize")
    @GetMapping
    public ResponseEntity<ProducerIntervalAwardsDto> findMaxAndMin() {
        ProducerIntervalAwardsDto intervalAwardsDto = service.fetchMaxAndMinIntervalPrizes();

        HttpStatus status = HttpStatus.OK;
        if (intervalAwardsDto.getMax().isEmpty() && intervalAwardsDto.getMin().isEmpty()) {
            status = HttpStatus.NO_CONTENT;
        }
        return new ResponseEntity(intervalAwardsDto, status);
    }
}

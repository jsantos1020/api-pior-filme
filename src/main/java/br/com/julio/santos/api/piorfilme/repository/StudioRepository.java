package br.com.julio.santos.api.piorfilme.repository;

import br.com.julio.santos.api.piorfilme.model.Studio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StudioRepository extends JpaRepository<Studio, Long> {

    Optional<Studio> findByName(String name);
}

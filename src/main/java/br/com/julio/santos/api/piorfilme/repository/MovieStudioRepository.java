package br.com.julio.santos.api.piorfilme.repository;

import br.com.julio.santos.api.piorfilme.model.MovieStudio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieStudioRepository extends JpaRepository<MovieStudio, Long> {
}

package br.com.julio.santos.api.piorfilme.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProducerIntervalAwardsDto {

    private List<ProducerIntervalDto> min;
    private List<ProducerIntervalDto> max;
}


package br.com.julio.santos.api.piorfilme.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProducerIntervalDto {

    private String producer;
    private Integer interval;
    private Integer previousWin;
    private Integer followingWin;
}

package br.com.julio.santos.api.piorfilme.service.impl;

import br.com.julio.santos.api.piorfilme.commons.exception.RegistroNaoEncontradoException;
import br.com.julio.santos.api.piorfilme.dto.MovieDto;
import br.com.julio.santos.api.piorfilme.model.Movie;
import br.com.julio.santos.api.piorfilme.repository.MovieRepository;
import br.com.julio.santos.api.piorfilme.service.api.MovieService;
import br.com.julio.santos.api.piorfilme.service.api.ProducerService;
import br.com.julio.santos.api.piorfilme.service.api.StudioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class MovieServiceImpl implements MovieService {

    @Autowired
    private MovieRepository repository;
    @Autowired
    private StudioService studioService;
    @Autowired
    private ProducerService producerService;

    @Override
    public MovieDto save(MovieDto movieDto) {
        Movie movie = save(Movie.builder().year(movieDto.getYear()).title(movieDto.getTitle()).winner(movieDto.getWinner()).build());
        List<String> studios = studioService.saveStudioAndMovie(movie, movieDto.getStudios());
        List<String> producers = producerService.saveProducerAndMovice(movie, movieDto.getProducers());

        return MovieDto.builder()
                    .id(movie.getId())
                    .year(movie.getYear())
                    .title(movie.getTitle())
                    .studios(studios)
                    .producers(producers)
                    .winner(movie.getWinner())
                .build();
    }

    @Transactional
    @Override
    public Movie save(Movie entity) {
        return repository.save(entity);
    }

    @Transactional
    @Override
    public void delete(Movie movie) {
        repository.delete(movie);
    }

    @Override
    public Movie findOne(Long id) {
        return repository.findById(id).orElseThrow(RegistroNaoEncontradoException::new);
    }

    @Override
    public String deleteMovie(Long id) {
        Movie movie = findOne(id);
        delete(movie);
        return new StringBuilder("O filme ").append(movie.getTitle()).append(" foi excluído!").toString();
    }
}

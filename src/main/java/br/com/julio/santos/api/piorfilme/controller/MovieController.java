package br.com.julio.santos.api.piorfilme.controller;

import br.com.julio.santos.api.piorfilme.dto.MovieDto;
import br.com.julio.santos.api.piorfilme.service.api.MovieService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/movies")
@Api(tags = { "Movie" }, description = "URIs for the Movies feature")
public class MovieController {

    @Autowired
    private MovieService service;

    @ApiOperation(value = "Create a movie")
    @PostMapping
    public ResponseEntity<MovieDto> create(@RequestBody MovieDto movieDto) {
        return new ResponseEntity(service.save(movieDto), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Delete a movie")
    @DeleteMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity delete(@ApiParam(value = "movie code", required = true, example = "10") @PathVariable Long id) {
        return ResponseEntity.ok(service.deleteMovie(id));
    }
}

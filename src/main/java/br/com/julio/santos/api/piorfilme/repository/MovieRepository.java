package br.com.julio.santos.api.piorfilme.repository;

import br.com.julio.santos.api.piorfilme.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {
}

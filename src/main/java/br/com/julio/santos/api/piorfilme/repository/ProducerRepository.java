package br.com.julio.santos.api.piorfilme.repository;

import br.com.julio.santos.api.piorfilme.model.Producer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProducerRepository extends JpaRepository<Producer, Long> {

    Optional<Producer> findByName(String producer);
}

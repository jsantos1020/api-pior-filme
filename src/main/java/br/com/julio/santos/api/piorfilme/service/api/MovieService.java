package br.com.julio.santos.api.piorfilme.service.api;

import br.com.julio.santos.api.piorfilme.commons.generic.GenericService;
import br.com.julio.santos.api.piorfilme.dto.MovieDto;
import br.com.julio.santos.api.piorfilme.model.Movie;

public interface MovieService extends GenericService<Movie, Long> {

    MovieDto save(MovieDto movieDto);
    String deleteMovie(Long id);
    void delete(Movie movie);
    Movie findOne(Long id);
}

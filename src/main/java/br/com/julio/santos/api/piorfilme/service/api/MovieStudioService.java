package br.com.julio.santos.api.piorfilme.service.api;

import br.com.julio.santos.api.piorfilme.commons.generic.GenericService;
import br.com.julio.santos.api.piorfilme.model.MovieStudio;

public interface MovieStudioService extends GenericService<MovieStudio, Long> {
}

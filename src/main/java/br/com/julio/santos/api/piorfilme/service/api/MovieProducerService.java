package br.com.julio.santos.api.piorfilme.service.api;

import br.com.julio.santos.api.piorfilme.commons.generic.GenericService;
import br.com.julio.santos.api.piorfilme.dto.ProducerIntervalAwardsDto;
import br.com.julio.santos.api.piorfilme.model.MovieProducer;

public interface MovieProducerService extends GenericService<MovieProducer, Long> {

    ProducerIntervalAwardsDto fetchMaxAndMinIntervalPrizes();
}

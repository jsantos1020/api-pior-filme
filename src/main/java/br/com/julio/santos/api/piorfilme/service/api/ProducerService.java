package br.com.julio.santos.api.piorfilme.service.api;

import br.com.julio.santos.api.piorfilme.commons.generic.GenericService;
import br.com.julio.santos.api.piorfilme.model.Movie;
import br.com.julio.santos.api.piorfilme.model.Producer;

import java.util.List;

public interface ProducerService extends GenericService<Producer, Long> {

    void saveProducerAndMovice(Movie movie, String producers);
    List<String> saveProducerAndMovice(Movie movie, List<String> producers);
}

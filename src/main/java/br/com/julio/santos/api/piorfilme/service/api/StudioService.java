package br.com.julio.santos.api.piorfilme.service.api;

import br.com.julio.santos.api.piorfilme.commons.generic.GenericService;
import br.com.julio.santos.api.piorfilme.model.Movie;
import br.com.julio.santos.api.piorfilme.model.Studio;

import java.util.List;

public interface StudioService extends GenericService<Studio, Long> {

    void saveStudioAndMovie(Movie movie, String studios);
    List<String> saveStudioAndMovie(Movie movie, List<String> studios);
}

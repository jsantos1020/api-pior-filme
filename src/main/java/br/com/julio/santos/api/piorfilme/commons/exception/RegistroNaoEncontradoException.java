package br.com.julio.santos.api.piorfilme.commons.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Não foi possível encontrar o registro")
public class RegistroNaoEncontradoException extends RuntimeException {
}

package br.com.julio.santos.api.piorfilme.commons.generic;

import br.com.julio.santos.api.piorfilme.commons.base.BaseEntity;
import br.com.julio.santos.api.piorfilme.commons.base.BaseService;

public interface GenericService<E extends BaseEntity, TypeId> extends BaseService {

    E save(E entity);
}

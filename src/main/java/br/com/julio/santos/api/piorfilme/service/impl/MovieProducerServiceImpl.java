package br.com.julio.santos.api.piorfilme.service.impl;

import br.com.julio.santos.api.piorfilme.dto.ProducerIntervalAwardsDto;
import br.com.julio.santos.api.piorfilme.dto.ProducerIntervalDto;
import br.com.julio.santos.api.piorfilme.model.MovieProducer;
import br.com.julio.santos.api.piorfilme.repository.MovieProducerRepository;
import br.com.julio.santos.api.piorfilme.service.api.MovieProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MovieProducerServiceImpl implements MovieProducerService {

    @Autowired
    private MovieProducerRepository repository;

    @Override
    public MovieProducer save(MovieProducer entity) {
        return repository.save(entity);
    }

    @Override
    public ProducerIntervalAwardsDto fetchMaxAndMinIntervalPrizes() {
        List<ProducerIntervalDto> intervalDtos = findProducersInterval(repository.findByMovieWinnerOrderById(true));

        return ProducerIntervalAwardsDto.builder().min(showMinInternal(intervalDtos)).max(showMaxInternal(intervalDtos)).build();
    }

    private List<ProducerIntervalDto> findProducersInterval(List<MovieProducer> list) {
        List<ProducerIntervalDto> intervalDtos = new ArrayList<>();

        for (int i = 0; i < list.size() - 1; i++) {
            for (int j = i + 1; j < list.size(); j++) {

                MovieProducer mpi = list.get(i);
                MovieProducer mpj = list.get(j);

                if (mpi.getProducer().equals(mpj.getProducer())) {
                    Integer interval = Math.abs(mpi.getMovie().getYear() - mpj.getMovie().getYear());
                    intervalDtos.add(ProducerIntervalDto.builder()
                                .interval(interval)
                                .producer(mpi.getProducer().getName())
                                .previousWin(mpi.getMovie().getYear())
                                .followingWin(mpj.getMovie().getYear())
                            .build());
                    break;
                }
            }
        }
        return intervalDtos;
    }

    private List<ProducerIntervalDto> showMaxInternal(List<ProducerIntervalDto> intervalDtos) {
        Integer max = intervalDtos.stream().mapToInt(ProducerIntervalDto::getInterval).max().orElseThrow(RuntimeException::new);
        return intervalDtos.stream().filter(p -> p.getInterval() == max).collect(Collectors.toList());
    }

    private List<ProducerIntervalDto> showMinInternal(List<ProducerIntervalDto> intervalDtos) {
        Integer min = intervalDtos.stream().mapToInt(ProducerIntervalDto::getInterval).min().orElseThrow(RuntimeException::new);
        return intervalDtos.stream().filter(p -> p.getInterval() == min).collect(Collectors.toList());
    }
}

package br.com.julio.santos.api.piorfilme.repository;

import br.com.julio.santos.api.piorfilme.model.MovieProducer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovieProducerRepository extends JpaRepository<MovieProducer, Long> {

    List<MovieProducer> findByMovieWinnerOrderById(Boolean winner);
}

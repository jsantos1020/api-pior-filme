package br.com.julio.santos.api.piorfilme;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PiorFilmeApplication {

    public static void main(String[] args) {
        SpringApplication.run(PiorFilmeApplication.class, args);
    }
}

package br.com.julio.santos.api.piorfilme.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class MovieDto {

    private Long id;
    private Integer year;
    private String title;
    private List<String> studios;
    private List<String> producers;
    private Boolean winner;
}

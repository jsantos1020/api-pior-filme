package br.com.julio.santos.api.piorfilme.controller;

import br.com.julio.santos.api.piorfilme.dto.MovieDto;
import br.com.julio.santos.api.piorfilme.generic.ApplicationIT;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class MovieControllerTest extends ApplicationIT {

    @Test
    @DisplayName("Save a movie")
    public void testSaveMovies() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.post("/movies").contentType(MediaType.APPLICATION_JSON).content(asJsonPostMovie())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.year").value(2015))
                .andExpect(jsonPath("$.winner").value(true));
    }

    @Test
    @DisplayName("Delete a movie")
    public void testDeleteMovies() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/movies/5").contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    private String asJsonPostMovie() {
        return asJsonString(MovieDto.builder().title("Teste")
                .year(2015)
                .winner(true)
                .studios(Arrays.asList("Studio 01", "Studio 02"))
                .producers(Arrays.asList("Producer 01", "Producer 02"))
                .build());
    }
}

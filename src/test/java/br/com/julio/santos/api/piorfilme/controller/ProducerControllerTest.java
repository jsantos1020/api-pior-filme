package br.com.julio.santos.api.piorfilme.controller;

import br.com.julio.santos.api.piorfilme.generic.ApplicationIT;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

public class ProducerControllerTest extends ApplicationIT {

    @Test
    @DisplayName("Search Interval Producers")
    public void testGetProducersInterval() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/producers").contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
